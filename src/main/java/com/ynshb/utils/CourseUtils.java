package com.ynshb.utils;

import com.ynshb.model.Course;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CourseUtils {

    public static List<Course> courses = new ArrayList<>();

    static {
        courses.add(Course.builder().id(1).name("Course 1").birthDate(LocalDate.now().plusDays(1)).build());
        courses.add(Course.builder().id(2).name("Course 2").birthDate(LocalDate.now().plusDays(2)).build());
        courses.add(Course.builder().id(3).name("Course 3").birthDate(LocalDate.now().plusDays(3)).build());
        courses.add(Course.builder().id(4).name("Course 4").birthDate(LocalDate.now().plusDays(4)).build());
        courses.add(Course.builder().id(5).name("Course 5").birthDate(LocalDate.now().plusDays(5)).build());
        courses.add(Course.builder().id(6).name("Course 6").birthDate(LocalDate.now().plusDays(6)).build());
    }
}
