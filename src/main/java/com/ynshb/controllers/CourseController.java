package com.ynshb.controllers;

import com.ynshb.exception.CustomException;
import com.ynshb.model.Course;
import com.ynshb.utils.CourseUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api")
public class CourseController {

    @GetMapping
    @RequestMapping("/courses")
    public List<Course> getAllCourses() {
        return CourseUtils.courses;
    }

    @GetMapping
    @RequestMapping("/course-by")
    public Course getCourseByParam(@RequestParam(required = true) Integer courseId) {
        Optional<Course> courseById = CourseUtils.courses.stream().filter(c -> c.getId().equals(courseId)).findAny();
        if (courseById.isEmpty()) {
            throw new CustomException("Course Not Found");
        }
        return courseById.get();
    }

    @PostMapping
    @RequestMapping("/course")
    public void addNewCourse(@RequestBody Course course) {
        Optional<Course> courseById = CourseUtils.courses.stream().filter(c -> c.getId().equals(course.getId())).findAny();
        if (courseById.isPresent()) {
            throw new CustomException("Course Already Exist");
        }
        CourseUtils.courses.add(course);
    }

    @GetMapping
    @RequestMapping("/course/{courseId}")
    public Course getCourseByPath(@PathVariable(name = "courseId") Integer courseId) {
        Optional<Course> courseById = CourseUtils.courses.stream().filter(c -> c.getId().equals(courseId)).findAny();
        if (courseById.isEmpty()) {
            throw new CustomException("Course Not Found");
        }
        return courseById.get();
    }
}
