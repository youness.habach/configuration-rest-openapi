package com.ynshb.controllers;

import com.ynshb.model.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/custom")
public class CustomController {

    @GetMapping
    public String getHelloString() {
        return "Hello";
    }

    @GetMapping("/do-something")
    public ResponseEntity<?> doSomething() {
        return new ResponseEntity<>(
                Response.builder()
                        .status(HttpStatus.OK.name())
                        .code(HttpStatus.OK.value())
                        .message("Success operation")
                        .build(), HttpStatus.OK
        );
    }
}
