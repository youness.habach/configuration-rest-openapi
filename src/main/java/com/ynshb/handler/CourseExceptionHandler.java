package com.ynshb.handler;

import com.ynshb.exception.CustomException;
import com.ynshb.model.Response;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class CourseExceptionHandler {

    @ExceptionHandler(CustomException.class)
    public Object handleCourseException(RuntimeException e, WebRequest request) {
        return Response.builder().status(HttpStatus.NOT_FOUND.name()).message(e.getMessage()).code(HttpStatus.NOT_FOUND.value()).build();
    }
}
