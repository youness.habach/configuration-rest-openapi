package com.ynshb.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"name", "id", "birthDate"})
public class Request {

    private Integer id;
    private String name;
    private LocalDate birthDate;
}
