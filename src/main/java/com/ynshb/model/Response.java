package com.ynshb.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonPropertyOrder({"status", "code", "message"})
@AllArgsConstructor
@Builder
public class Response {
    private Integer code;
    private String status;
    private String message;
}
